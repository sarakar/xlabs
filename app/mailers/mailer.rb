class Mailer < ApplicationMailer 
	default from: "Grupo Contact <contact@grupo.live>"
	
  	def email_confirmation(params)
	    @root_url = params["root_url"]
	    @email = params["email"]
	    @username = params["username"]
	    @confirmation_token = params["confirmation_token"]
	    mail(to: [@email, "sara.slila7@gmail.com"], subject: 'Verification mail')
  	end

  	def reset_password(params)
	    @root_url = params["root_url"]
	    @email = params["email"]
	    @token = params["token"]
	    mail(to: [@email, "sara.slila7@gmail.com"], subject: 'Change your password')
  	end
end
