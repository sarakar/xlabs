class Api::GlobalController  < ActionController::Base

	def send_grupo_mail
		if params["mail_type"].present? and params["mail_type"].to_i == 0
			Mailer.email_confirmation(params).deliver
		elsif params["mail_type"].present? and params["mail_type"].to_i == 1
			Mailer.reset_password(params).deliver
		end
	  	render json: {success: 0}
	end
end