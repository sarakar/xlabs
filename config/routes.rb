Rails.application.routes.draw do
  #web
  root 'global#index'

  namespace :api, :defaults => {:format => 'json'} do
  	post '/send_grupo_mail', to: 'global#send_grupo_mail'
  end 
  

end
